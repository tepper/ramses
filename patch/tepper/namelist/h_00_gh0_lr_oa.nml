! COMPILE CODE WITH: Makefile_3D_GNU_OLDMPI0_DICEMULTIPATCH_GAS
!
! ICs created with my AGAMA + gas framework
!
! MAKE SURE to set the correct path to the ICs (initfile)
!
! The difference between this NML file and h_00_gh0_lr.nml is that the latter corresponds to my setup, while this corresponds to O.Agerzt (e.g. fgas10_agama_rf) setup without galaxy formation sub-grid physics (i.e. star formation, etc.). See also h_00_gh0_lr_star.

&RUN_PARAMS
verbose=.false.
cosmo=.false.
pic=.true.
hydro=.true.
poisson=.true.
!tracer=.false.		! not a standard Ramses parameter
ncontrol=1
nremap=7
nrestart=0
restart_remap=.true.
nstepmax=10000000
nsubcycle=1,1,2,2,2,2,2,2,2,2,2
ordering='hilbert'
rt=.false.
/

! Note that these differ from my usual settings and will affect the value of the time unit, prompting changes to OUTPUT_PARAMS
&UNITS_PARAMS
units_density = 1.573391528d-26			! g/cm^3 = 1 H / cm^3
units_time    = 3.00386047142391D+15 	! s; irrelevant if poisson=.true.
units_length  = 3.0856776D+21 			! cm = 1 kpc
/

&COOLING_PARAMS
cooling=.true.
/

&HYDRO_PARAMS
pressure_fix=.true.
gamma=1.666666667
courant_factor=0.9
slope_type=1
riemann='hllc'
beta_fix=0.5
/

! Memory management PER CORE (pure N-body runs)
! ~ 0.8 * [ (ngridmax/10^6) + (npartmax/10^7) ] GB
! Note that the more cores, the smaller ngridmax and npartmax can be
&AMR_PARAMS
levelmin=7
levelmax=13
!ngridmax=3000000
!npartmax=4000000
ngridmax=800000 ! ~ 0.64 GB/core
npartmax=1500000 ! ~ 0.15 GB/core
boxlen=600.0
/

!initfile must be such that expanded by the IC file names
!collectively has len < 80 characters
!the longest file name reads:
!initfile(1)output_xxxxx/hydro_file_descriptor.txt
!check: $> echo initfile(1)/output_xxxxx/hydro_file_descriptor.txt | wc -c
&INIT_PARAMS
filetype='dice'
!initfile(1)='/g/data/aa8/ttg562/h_00_gh0_lr_oa/ic/'
initfile(1)='/import/photon2/tepper/h_00_gh0_lr_oa/ic/'
!initfile(1)='/project/GalSims/h_00_gh0_lr_oa/ic/'
d_region=1.0d-6
p_region=1.0d6
/

!output_dir name must have len < 40 characters to accomodate
!the longest Ramses output file names
!check: $> echo output_dir | wc -c
&OUTPUT_PARAMS
tend=2.0
delta_tout=0.01
walltime_hrs=1.0
minutes_dump=5
!output_dir='/g/data/aa8/ttg562/h_00_gh0_lr_oa/'
output_dir='/import/photon2/tepper/h_00_gh0_lr_oa/'
!output_dir='/project/GalSims/h_00_gh0_lr_oa/'
/

&REFINE_PARAMS
interpol_var=0
interpol_type=0
mass_sph=5.d-2 !2.d-4 !2.d-4 !refine when mass_sph is 8 times mass_sph. Remember unit of mass is ~2.35e5 Msun
m_refine=10*8
x_refine=300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0
y_refine=300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0
z_refine=300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0
r_refine=80.0,80.0,80.0,80.0,80.0,80.0,80.0,80.0,80.0,80.0,80.0
/

&POISSON_PARAMS
gravity_type=0
epsilon=5.0e-4
/

&DICE_PARAMS
ic_file='h_00_gh0_lr_proc.g2' ! initial conditions file
ic_nfile=2                    ! If >1, look for files with name ic_file//'.n'
ic_ifout=1                    ! Change ramses output index for restarts
ic_format='Gadget2'           ! Format of the initial conditions
!ic_scale_metal=0.02           ! Metallicity scaling (solar value)
ic_head_name='HEAD'           ! Header datablock (Gadget2, standard)
ic_pos_name='POS '            ! position vector datablock (Gadget2, standard)
ic_vel_name='VEL '            ! velocity vector datablock (Gadget2, standard)
ic_mass_name='MASS'           ! mass datablock (Gadget2, standard)
ic_id_name='ID  '             ! particle ID datablock (Gadget2, standard)
ic_u_name='U   '              ! internal energy datablock (Gadget2, standard)
ic_metal_name='ZMET'          ! metallicity datablock (Gadget2, *custom*)
ic_age_name='AGE '            ! particle age datablock (Gadget2, standard)
IG_rho=5.4D-6                 ! Intergalactic gas density; the adopted value is ~rho at the halo's edge
IG_T2=6.8D5                   ! Intergalactic gas temperature (SHOULD be T - 4.5e5 at the halo's edge
IG_metal=0.0D0                 ! Intergalactic gas metallicity (solar units)
gadget_scale_l=3.085677581282D+21	! AGAMA file length unit   -> 1 kpc
gadget_scale_v=1.0D+5 				! AGAMA file velocity unit -> 1 km/s
gadget_scale_m=1.9891D+33			! AGAMA file mass unit     -> 1 solar mass
gadget_scale_t=3.15360D+13			! Gadget file time unit    -> 1 Myr (NOT the simulation time unit)
/
