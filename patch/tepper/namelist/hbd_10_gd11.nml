! Compile the code with:
! /Users/tepper/codes/ramses_fork/bin/Makefile_3D_GNU_OLDMPI0_DICEMULTIPATCH_GAS
!

&RUN_PARAMS
print_when_io=.true.
verbose=.false.
cosmo=.false.
pic=.true.
hydro=.true.
poisson=.true.
tracer=.false.
ncontrol=1
nremap=7
nrestart=0
restart_remap=.true.
nstepmax=10000000
nsubcycle=1,1,2,2,2,2,2,2,2,2,2
ordering='hilbert'
rt=.false.
/


! Note that these differ from my usual settings and will affect the value of the time unit, prompting changes to OUTPUT_PARAMS
&UNITS_PARAMS
units_density = 1.573391528d-26			! g/cm^3 = 1 H / cm^3
units_time    = 3.00386047142391D+15 	! s; irrelevant if poisson=.true.
units_length  = 3.0856776D+21 			! cm = 1 kpc
/


! *strictly* isothermal, which means that IG_T2 will be ignored, and all the gas will have T_eos/mu_gas
&COOLING_PARAMS
barotropic_eos = .true.
barotropic_eos_form = 'isothermal'
T_eos = 2.0d3
mu_gas = 1.0d0
/


&HYDRO_PARAMS
pressure_fix=.true.
gamma=1.666666667
courant_factor=0.9
slope_type=1
riemann='hllc'
beta_fix=0.5
/


! Memory management PER CORE (N-body+HD runs)
! ~ 0.8*[ 2*(ngridmax/10^6) + (npartmax/10^7) ] GB
! The ICs used here (DICE_PARAMS) have roughly 3.5 million particles
! Note that the more cores, the smaller ngridmax and npartmax can be
&AMR_PARAMS
boxlen=6.0d2
levelmin=7
levelmax=14 !13
ngridmax=200000
npartmax=3000000
nexpand=1
/


&REFINE_PARAMS
mass_sph=5.d-2
m_refine=10*8
interpol_type=0
interpol_var=0
/


&BOUNDARY_PARAMS
nboundary=6
bound_type= 2, 2,  2,  2,  2,  2 ! zero-gradient BCs ...
ibound_min=-1, 1, -1, -1, -1, -1
ibound_max=-1, 1,  1,  1,  1,  1
jbound_min= 0, 0, -1,  1, -1, -1
jbound_max= 0, 0, -1,  1,  1,  1
kbound_min= 0, 0,  0,  0, -1,  1
kbound_max= 0, 0,  0,  0, -1,  1
no_inflow=.false.				  ! ...  with inflow
/


&INIT_PARAMS
filetype='dice'
initfile(1)='/import/photon2/tepper/hbd_10_gd11/ic/'
!initfile(1)='/scratch/dr86/ttg562/hbd_10_gd11/ic/'
/


!MAKE SURE TO USE THE CORRECT UNITS FOR length, velocity, and mass
!For more parameter see:
!https://bitbucket.org/vperret/dice/wiki/RAMSES%20simulation
&DICE_PARAMS
ic_file='hbd_10_gd11_proc.g2'           ! Name of the initial conditions file
ic_nfile=4                    ! If greater than one, look for files with name matching ic_file//'.n'
ic_format='Gadget2'           ! Format of the initial conditions. 'Gadget1' or 'Gadget2'
ic_scale_metal=0.02           ! Scaling factor for the metallicity (DICE adopts solar units, RAMSES expects actual metal mass frac.)
ic_metal_name='ZMET'          ! Name of the metallicity datablock (Gadget2 format only; default is 'Z   ')
IG_rho=1.0D-20                ! IGM gas density (default 1.0d-5)
IG_T2=2.0D3                   ! IGM gas temperature (default: 1.0d7)
IG_metal=0.3                  ! IGM metallicity (default: 1.0d-2)
gadget_scale_l=3.085677581282D+21	! AGAMA file length unit   -> 1 kpc
gadget_scale_v=1.0D+5 				! AGAMA file velocity unit -> 1 km/s
gadget_scale_m=1.9891D+33			! AGAMA file mass unit     -> 1 solar mass
gadget_scale_t=3.15360D+13			! Gadget file time unit    -> 1 Myr (note that this is NOT the simulation time unit)
/


!output_dir name must have len < 40 characters to accomodate
!the longest Ramses output file names
!check: $> echo output_dir | wc -c
&OUTPUT_PARAMS
tend=2.0
delta_tout=0.001
walltime_hrs=48.0
minutes_dump=1
output_dir='/import/photon2/tepper/hbd_10_gd11/'
!output_dir='/scratch/dr86/ttg562/hbd_10_gd11/'
!output_dir='/g/data/aa8/ttg562/hbd_10_gd11/'
/
