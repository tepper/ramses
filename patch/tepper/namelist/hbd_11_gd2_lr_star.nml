! IMPORTANT: This code is missing the BOUNDARY_PARAMS block!!!!
! I'm not sure how this may affect the system's evolution
!
!This namelist contains various input parameters for RAMSES runs
!
! Compile the code with:
! /Users/tepper/codes/ramses_fork/bin/Makefile_3D_GNU_OLDMPI0_OAGERTZ_DICEPATCH
!
! ICs created with my AGAMA + gas framework
!
! MAKE SURE to set the correct path to the ICs (initfile)
!
! IMPORTANT: THIS MODEL does not have significant star formation with the settings as they are, likely due to the low (central) gas density; see model hbd_11_gd11_lr_star instead.

&RUN_PARAMS
verbose=.false.
cosmo=.false.
pic=.true.
hydro=.true.
poisson=.true.
!tracer=.false. -> not available in this patch
ncontrol=1
nremap=7
nrestart=0
restart_remap=.true.
nstepmax=10000000
nsubcycle=1,1,2,2,2,2,2,2,2,2,2
ordering='hilbert'
rt=.false.
/

! Note that these differ from my usual settings and will affect the value of the time unit, prompting changes to OUTPUT_PARAMS
&UNITS_PARAMS
units_density = 1.573391528d-26			! g/cm^3 = 1 H / cm^3
units_time    = 3.00386047142391D+15 	! s; irrelevant if poisson=.true.
units_length  = 3.0856776D+21 			! cm = 1 kpc
/

&FEEDBACK_PARAMS
f_w=0.
eta_sn=0.1
SNenergy=1.0d51
Tmax=5.0d8				! could be anything between 1e7 - 1e9
radpressure=.true.		! DO
eta_rap=2.0d0			! NOT
tau_IR=-1.				! CHANGE
vmaxFB=4000.00
maxadvfb=4000.0
smallT=1.e0
fbsafety=.true.			! avoid runaway mom.injection
Nrcool=6.0				! cells; could 3 - 6
supernovae=.true.
winds=.true.
momentum=.true.
energy=.true.
metalscaling=.true.
SNIamodel=1      		!DTD, t^-1.12
Ia_rate=2.6d-13  		!Field DTD
SNdiagnostics=.false. 	!not needed for now (what is it good for?)
/

&SF_PARAMS
mstarparticle=1.0d3		! Msun, initial mass
eps_star=0.1
n_star=10.0
temp_star=100.0			! irrelevant parameter
T2_star=0.1d0			! temperature floor (K)
g_star=2.0
/

&COOLING_PARAMS
cooling=.true.
neq_chem=.false.
haardt_madau=.true.
z_reion=8.5
metal=.true.
self_shielding=.true.
T2max=5.0d8
/

&HYDRO_PARAMS
pressure_fix=.true.
gamma=1.666666667
courant_factor=0.9
slope_type=1
riemann='hllc'
beta_fix=0.5
/

! Memory management PER CORE (pure N-body runs)
! ~ 0.8 * [ (ngridmax/10^6) + (npartmax/10^7) ] GB
! Note that the more cores, the smaller ngridmax and npartmax can be
&AMR_PARAMS
levelmin=7
levelmax=13
!ngridmax=3000000
!npartmax=4000000
ngridmax=800000 ! ~ 0.64 GB/core
npartmax=1500000 ! ~ 0.15 GB/core
boxlen=600.0
/

!initfile must be such that expanded by the IC file names
!collectively has len < 80 characters
!the longest file name reads:
!initfile(1)output_xxxxx/hydro_file_descriptor.txt
!check: $> echo initfile(1)/output_xxxxx/hydro_file_descriptor.txt | wc -c
&INIT_PARAMS
filetype='dice'
!initfile(1)='/g/data/aa8/ttg562/hbd_11_gd2_lr_star/ic/'
initfile(1)='/import/photon2/tepper/hbd_11_gd2_lr_star/ic/'
!initfile(1)='/project/GalSims/hbd_11_gd2_lr_star/ic/'
d_region=1.0d-6
p_region=1.0d6
/

!output_dir name must have len < 40 characters to accomodate
!the longest Ramses output file names
!check: $> echo output_dir | wc -c
&OUTPUT_PARAMS
!noutput=200
!tend=1.0
!delta_tout=0.005
tend=4.5
delta_tout=0.01
walltime_hrs=1.0
minutes_dump=5
!output_dir='/g/data/aa8/ttg562/hbd_11_gd2_lr_star/'
output_dir='/import/photon2/tepper/hbd_11_gd2_lr_star/'
!output_dir='/project/GalSims/hbd_11_gd2_lr_star/'
/

&REFINE_PARAMS
interpol_var=0
interpol_type=0
mass_sph=5.d-2 !2.d-4 !2.d-4 !refine when mass_sph is 8 times mass_sph. Remember unit of mass is ~2.35e5 Msun
m_refine=10*8
x_refine=300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0
y_refine=300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0
z_refine=300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0,300.0
r_refine=80.0,80.0,80.0,80.0,80.0,80.0,80.0,80.0,80.0,80.0,80.0
/

&POISSON_PARAMS
gravity_type=0
epsilon=5.0e-4
/

!MAKE SURE TO USE THE CORRECT UNITS FOR length, velocity, and mass
!For more parameter see:
!https://bitbucket.org/vperret/dice/wiki/RAMSES%20simulation
&DICE_PARAMS
ic_file='hbd_11_gd2_lr_proc.g2'           ! Name of the initial conditions file
ic_nfile=4                    ! If greater than one, look for files with name matching ic_file//'.n'
ic_ifout=1                    ! Change ramses output index for restarts
ic_format='Gadget2'           ! Format of the initial conditions. 'Gadget1' or 'Gadget2'
ic_center=0.0,0.0,0.0         ! Shift center parameter. ICs are automatically shifted with boxlen/2
ic_scale_pos=1.0              ! Scaling factor for the position vector
ic_scale_vel=1.0              ! Scaling factor for the velocity vector
ic_scale_mass=1.0             ! Scaling factor for the mass
ic_scale_u=1.0                ! Scaling factor for the internal energy
ic_scale_age=1.0              ! Scaling factor for the particles age
ic_scale_metal=0.02           ! Scaling factor for the metallicity (DICE adopts solar units, RAMSES expects actual metal mass frac.)
ic_head_name='HEAD'           ! Name of the Header datablock (Gadget2 format only)
ic_pos_name='POS '            ! Name of the position vector datablock (Gadget2 format only)
ic_vel_name='VEL '            ! Name of the velocity vector datablock (Gadget2 format only)
ic_mass_name='MASS'           ! Name of the mass datablock (Gadget2 format only)
ic_id_name='ID  '             ! Name of the particle identifier datablock (Gadget2 format only)
ic_u_name='U   '              ! Name of the internal energy datablock (Gadget2 format only)
ic_metal_name='ZMET'          ! Name of the metallicity datablock (Gadget2 format only)
ic_age_name='AGE '            ! Name of the particle age datablock (Gadget2 format only)
IG_rho=1.0D-6                 ! IGM gas density (default 1.0d-5)
IG_T2=1.0D6                    ! IGM gas temperature (default: 1.0d7)
IG_metal=0.0                  ! IGM metallicity (defaul:t 1.0d-2)
amr_struct=.false.            ! Reproduce the AMR structure of the Gadget2 file resulting from a ramses to gadget conversion
gadget_scale_l=3.085677581282D+21	! AGAMA file length unit   -> 1 kpc
gadget_scale_v=1.0D+5 				! AGAMA file velocity unit -> 1 km/s
gadget_scale_m=1.9891D+33			! AGAMA file mass unit     -> 1 solar mass
gadget_scale_t=3.15360D+13			! Gadget file time unit    -> 1 Myr (note that this is NOT the simulation time unit)
/
