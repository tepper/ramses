! Compile the code with:
!
! /Users/tepper/codes/ramses_fork/bin/Makefile_3D_GNU_OLDMPI0_OAGERTZ_DICEPATCH
!
! ICs created with my AGAMA + gas framework
!
! MAKE SURE to set the correct path to the ICs (initfile)
!
! A copy of hizb_07_lr_fgas20_star.nml to test Ramses scaling capabilities
!
! Removed MOVIE_PARAMS block!


&RUN_PARAMS
verbose=.false.
cosmo=.false.
pic=.true.
hydro=.true.
poisson=.true.
!tracer=.false. -> not available in this patch
ncontrol=1
nremap=7
nrestart=0
restart_remap=.true.
nstepmax=10000000
nsubcycle=1,1,2,2,2,2,2,2,2,2,2
ordering='hilbert'
rt=.false.
/

! INCLUDED POST 12 FEB 2023 to mimick O.Agertz setup units.f90; the latter has been inactivated
! Note that these differ from my usual settings and will affect the value of the time unit, prompting changes to OUTPUT_PARAMS
&UNITS_PARAMS
units_density = 1.573391528d-26			! g/cm^3 = 1 H / cm^3
units_time    = 3.00386047142391D+15 	! s; irrelevant if poisson=.true.
units_length  = 3.0856776D+21 			! cm = 1 kpc
/

&FEEDBACK_PARAMS
f_w=0.
eta_sn=0.1
SNenergy=1.0d51
Tmax=2.0d8				! was 5.0d8; could be anything between 1e7 - 1e9
radpressure=.true.		! DO
eta_rap=2.0d0			! NOT
tau_IR=-1.				! CHANGE
vmaxFB=2500.00			! was 4000.00
maxadvfb=2500.00			! was 4000.00
smallT=1.e0
fbsafety=.true.			! avoid runaway mom.injection
Nrcool=6.0				! cells; could 3 - 6
supernovae=.true.
winds=.true.
momentum=.true.
energy=.true.
metalscaling=.true.
SNIamodel=1      		!DTD, t^-1.12
Ia_rate=2.6d-13  		!Field DTD
SNdiagnostics=.false. 	!not needed for now (what is it good for?)
/

&SF_PARAMS
mstarparticle=1.0d3		! Msun, initial mass
eps_star=0.1
n_star=10.0
temp_star=100.0			! irrelevant parameter
T2_star=0.1d0			! temperature floor (K)
g_star=2.0
/

&COOLING_PARAMS
cooling=.true.
neq_chem=.false.
haardt_madau=.true.
z_reion=8.5
metal=.true.
self_shielding=.true.
T2max=2.0d8			! was 5.0d8
/

&HYDRO_PARAMS
pressure_fix=.true.
gamma=1.666666667
courant_factor=0.9
slope_type=1
riemann='hllc'
beta_fix=0.5
/

! Memory management PER CORE (pure N-body runs)
! ~ 0.8 * [ (ngridmax/10^6) + (npartmax/10^7) ] GB
! Note that the more cores, the smaller ngridmax and npartmax can be
&AMR_PARAMS
levelmin=7		! is 8 in hizb_07
levelmax=12		! is 14 in hizb_07 and 13 in hbd_19_gd8_lr_star
ngridmax=800000 ! ~ 0.64 GB/core
npartmax=1500000 ! ~ 0.15 GB/core
boxlen=100.0 ! was 600 before
/

!initfile must be such that expanded by the IC file names
!collectively has len < 80 characters
!the longest file name reads:
!initfile(1)output_xxxxx/hydro_file_descriptor.txt
!check: $> echo initfile(1)/output_xxxxx/hydro_file_descriptor.txt | wc -c
&INIT_PARAMS
filetype='dice'
initfile(1)='/g/data/aa8/ttg562/scaling_nbody_star/ic/'
d_region=1.0d-6
p_region=1.0d6
/

!output_dir name must have len < 40 characters to accomodate
!the longest Ramses output file names
!check: $> echo output_dir | wc -c
&OUTPUT_PARAMS
tend=0.01
delta_tout=0.002
output_dir='/g/data/aa8/ttg562/scaling_nbody_star/3node/'
/

! All of xyz_refine were 300 before
! All of r_refine were 80 before
! Changed in accord to boxlen
&REFINE_PARAMS
interpol_var=0
interpol_type=0
mass_sph=5.d-2 !2.d-4 !2.d-4 !refine when mass_sph is 8 times mass_sph. Remember unit of mass is ~2.35e5 Msun
m_refine=10*8
x_refine=50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0
y_refine=50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0
z_refine=50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0,50.0
r_refine=15.0,15.0,15.0,15.0,15.0,15.0,15.0,15.0,15.0,15.0,15.0
/

&POISSON_PARAMS
gravity_type=0
epsilon=5.0e-4
/

!MAKE SURE TO USE THE CORRECT UNITS FOR length, velocity, and mass
!For more parameter see:
!https://bitbucket.org/vperret/dice/wiki/RAMSES%20simulation
&DICE_PARAMS
ic_file='hizb_07_lr_fgas20_proc.g2'           ! Name of the initial conditions file
ic_nfile=4                    ! If greater than one, look for files with name matching ic_file//'.n'
ic_ifout=1                    ! Change ramses output index for restarts
ic_format='Gadget2'           ! Format of the initial conditions
ic_scale_metal=0.02           ! Metallicity scaling (solar value)
ic_head_name='HEAD'           ! Header datablock (Gadget2, standard)
ic_pos_name='POS '            ! position vector datablock (Gadget2, standard)
ic_vel_name='VEL '            ! velocity vector datablock (Gadget2, standard)
ic_mass_name='MASS'           ! mass datablock (Gadget2, standard)
ic_id_name='ID  '             ! particle ID datablock (Gadget2, standard)
ic_u_name='U   '              ! internal energy datablock (Gadget2, standard)
ic_metal_name='ZMET'          ! metallicity datablock (Gadget2, *custom*)
ic_age_name='AGE '            ! particle age datablock (Gadget2, standard)
IG_rho=1.0D-6                 ! IGM gas density (default 1.0d-5)
IG_T2=1.0D6                    ! IGM gas temperature (default: 1.0d7)
IG_metal=0.3                  ! IGM metallicity (solar units)
amr_struct=.false.            ! Reproduce the AMR structure of the Gadget2 file resulting from a ramses to gadget conversion
gadget_scale_l=3.085677581282D+21	! AGAMA file length unit   -> 1 kpc
gadget_scale_v=1.0D+5 				! AGAMA file velocity unit -> 1 km/s
gadget_scale_m=1.9891D+33			! AGAMA file mass unit     -> 1 solar mass
gadget_scale_t=3.15360D+13			! Gadget file time unit    -> 1 Myr (note that this is NOT the simulation time unit)
/
