! COMPILE CODE WITH: Makefile_3D_GNU_OLDMPI0_DICEMULTIPATCH_GAS
&RUN_PARAMS
verbose=.false.
print_when_io=.true.
cosmo=.false.
hydro=.true.
pic=.true.
poisson=.true.	! requires to set units such that G=1; taken care of by units.f90
nrestart=1
nremap=10
nsubcycle=8*1
ncontrol=1
/


&UNITS_PARAMS
units_density = 1.6605390D-24         ! g/cm^3 = 1 H / cm^3;	 Ramses unit
units_time    = 3.00386047142391D+15  ! s; -> G=1; needed if poisson=.true.
units_length  = 3.0856776D+21         ! cm = 1 kpc;	Ramses unit
/


&COOLING_PARAMS
barotropic_eos = .true.
barotropic_eos_form = 'isothermal'
T_eos = 1.0d4		!-> WRONG! Should be 1.0d3
mu_gas = 1.0d0
/


&HYDRO_PARAMS
smallr=1.0d-20			! may not have an effect due to IG_rho (see DICE_PARAMS)
gamma=1.667d0			! make sure it is identical to value in AGAMA preproc
courant_factor=0.5
slope_type=1
scheme='muscl'
riemann='llf'
pressure_fix=.true.
beta_fix=0.5d0
/


&POISSON_PARAMS
gravity_type=0	! requires to set units such that G=1
/


!Memory management:
!1.4(ngridmax/10^6) + 0.7(npartmax/10^7) Gb for N-body and hydro runs per core
! Note that the more cores, the smaller ngridmax and npartmax can be
&AMR_PARAMS
boxlen=6.0d2
levelmin=7
levelmax=14
!ngridmax=500000   ! on 240 cpus @ ~170GB;
!npartmax=11000000 ! on 240 cpus @ ~185GB;
!ngridmax=200000   ! on 24 cpus @ ~7GB; use this to start
!npartmax=20000000 ! on 24 cpus @ ~34GB; use this to start
ngridmax=150000   ! on 24 cpus @ ~5GB;  use this to restart
npartmax=4500000 ! on 24 cpus @ ~7.6GB; use this to restart
nexpand=1
/


&REFINE_PARAMS
jeans_refine=7*4		! WRONG! Should be 8*
mass_sph=2.5d-3   		!-> refinement runaway w/o interpolation scheme below
m_refine=8*25
interpol_type=0   		! IMPORTANT: use this to avoid refinement runaway
interpol_var=1    		! IMPORTANT: use this to avoid refinement runaway
/


&BOUNDARY_PARAMS
nboundary=6
bound_type= 2, 2,  2,  2,  2,  2 ! zero-gradient BCs ...
ibound_min=-1, 1, -1, -1, -1, -1
ibound_max=-1, 1,  1,  1,  1,  1
jbound_min= 0, 0, -1,  1, -1, -1
jbound_max= 0, 0, -1,  1,  1,  1
kbound_min= 0, 0,  0,  0, -1,  1
kbound_max= 0, 0,  0,  0, -1,  1
no_inflow=.false.				  ! ...  with inflow
/


!initfile must be such that expanded by the IC file names
!collectively has len < 80 characters
!the longest file name reads:
!initfile(1)output_xxxxx/hydro_file_descriptor.txt
&INIT_PARAMS
filetype='dice'
!initfile(1)='/scratch/dr86/ttg562/hbd_10_gd2_hd/ic/'
initfile(1)='/import/photon2/tepper/hbd_10_gd2_hd/ic/'
!initfile(1)='/project/GalSims/hbd_10_gd2_hd/ic/'
/


!MAKE SURE TO USE THE CORRECT UNITS FOR length, velocity, and mass
&DICE_PARAMS
ic_file='hbd_10_gd2_hd_proc.g2'	  ! Name of the initial conditions file
ic_nfile=4                    ! If greater than one, look for files with name matching ic_file//'.n'
ic_ifout=1                    ! Change ramses output index for restarts
ic_format='Gadget2'           ! Format of the initial conditions. 'Gadget1' or 'Gadget2'
gadget_scale_l=3.085677581282D+21 ! AGAMA file length unit   -> 1 kpc
gadget_scale_v=1.0D+5             ! AGAMA file velocity unit -> 1 km/s
gadget_scale_m=1.9891D+33         ! AGAMA file mass unit     -> 1 solar mass
gadget_scale_t=3.15360D+13        ! Gadget file time unit    -> 1 Myr (note that this is NOT the simulation time unit)
IG_rho=1.0d-20                 ! IGM gas density (default 1.0d-5)
IG_T2=1.0d3                    ! IGM gas temperature (default: 1.0d7)
IG_metal=0.0                   ! IGM metallicity (defaul:t 1.0d-2)
/


!output_dir name must have len < 40 characters to accomodate
!the longest Ramses output file names
&OUTPUT_PARAMS
tend = 10.
delta_tout = 0.1
!output_dir='/scratch/dr86/ttg562/hbd_10_gd2_hd_eos/'
output_dir='/import/photon2/tepper/hbd_10_gd2_hd_eos/'
!output_dir='/project/GalSims/hbd_10_gd2_hd_eos/'
/
