! COMPILE CODE WITH: Makefile_3D_GNU_OLDMPI0_DICEMULTIPATCH_GAS
!
! ICs created with my AGAMA + gas framework
! They are identical to h_00_gh0_lr's
!
! This NML is similar to h_00_gh0_lr, but without cooling (i.e adiabatic)
!
! MAKE SURE to set the correct path to the ICs (initfile)
!

&RUN_PARAMS
print_when_io=.true.
verbose=.false.
cosmo=.false.
hydro=.true.
pic=.true.
poisson=.true.
nrestart=0
nremap=10
nsubcycle=7*1
ncontrol=1
/

! Note that these differ from my usual settings and will affect the value of the time unit, prompting changes to OUTPUT_PARAMS
&UNITS_PARAMS
units_density = 1.6727d-24 ! 1 H / cm^3
units_time    = 0.470430312423675E+15 ! G=1 -> 14.9 Myr
units_length  = 0.308567758128200E+22 ! 1 kpc
/

&COOLING_PARAMS
cooling=.false.
/

&HYDRO_PARAMS
gamma=1.667d0
courant_factor=0.5
slope_type=3
scheme='muscl'
riemann='hllc'
pressure_fix=.true.
beta_fix=0.5d0
/

&POISSON_PARAMS
gravity_type=0
cic_levelmax=11	! Not really needed I guess...
/

&AMR_PARAMS
boxlen=5.0d2
levelmin=7
levelmax=13
ngridmax=500000
npartmax=2200000
nexpand=1
/

&REFINE_PARAMS
mass_sph=1.0d-3
m_refine=6*40
interpol_type=0
interpol_var=1
/

&BOUNDARY_PARAMS
nboundary=6
bound_type= 2, 2,  2,  2,  2,  2
ibound_min=-1, 1, -1, -1, -1, -1
ibound_max=-1, 1,  1,  1,  1,  1
jbound_min= 0, 0, -1,  1, -1, -1
jbound_max= 0, 0, -1,  1,  1,  1
kbound_min= 0, 0,  0,  0, -1,  1
kbound_max= 0, 0,  0,  0, -1,  1
no_inflow=.false.
/

&INIT_PARAMS
filetype='dice'
!initfile(1)='/g/data/aa8/ttg562/h_00_gh0_lr_ad/ic/'
initfile(1)='/import/photon2/tepper/h_00_gh0_lr_ad/ic/'
!initfile(1)='/project/GalSims/h_00_gh0_lr_ad/ic/'
/

&DICE_PARAMS
ic_file='h_00_gh0_lr_proc.g2' ! initial conditions file
ic_nfile=2                    ! If >1, look for files with name ic_file//'.n'
ic_ifout=1                    ! Change ramses output index for restarts
ic_format='Gadget2'           ! Format of the initial conditions
ic_head_name='HEAD'           ! Header datablock (Gadget2, standard)
ic_pos_name='POS '            ! position vector datablock (Gadget2, standard)
ic_vel_name='VEL '            ! velocity vector datablock (Gadget2, standard)
ic_mass_name='MASS'           ! mass datablock (Gadget2, standard)
ic_id_name='ID  '             ! particle ID datablock (Gadget2, standard)
ic_u_name='U   '              ! internal energy datablock (Gadget2, standard)
ic_metal_name='ZMET'          ! metallicity datablock (Gadget2, *custom*)
ic_age_name='AGE '            ! particle age datablock (Gadget2, standard)
IG_rho=5.4D-6                 ! Intergalactic gas density; the adopted value is ~rho at the halo's edge
IG_T2=6.8D5                   ! Intergalactic gas temperature (SHOULD be T - 4.5e5 at the halo's edge
IG_metal=0.0D0                 ! Intergalactic gas metallicity (solar units)
gadget_scale_l=3.085677581282D+21	! AGAMA file length unit   -> 1 kpc
gadget_scale_v=1.0D+5 				! AGAMA file velocity unit -> 1 km/s
gadget_scale_m=1.9891D+33			! AGAMA file mass unit     -> 1 solar mass
gadget_scale_t=3.15360D+13			! Gadget file time unit    -> 1 Myr (NOT the simulation time unit)
/

&OUTPUT_PARAMS
tend = 100.
delta_tout = 0.1
!output_dir='/g/data/aa8/ttg562/h_00_gh0_lr_ad/'
output_dir='/import/photon2/tepper/h_00_gh0_lr_ad/'
!output_dir='/project/GalSims/h_00_gh0_lr_ad/'
/
