! COMPILE CODE WITH: Makefile_3D_GNU_OLDMPI0_DICEMULTIPATCH
! OR: Makefile_3D_GNU_OLDMPI0_DICEPATCH
&RUN_PARAMS
verbose=.false.
print_when_io=.true.
cosmo=.false.
hydro=.false.
pic=.true.
poisson=.true.	! requires to set units such that G=1; taken care of by units.f90
sink=.true.
nrestart=0
nremap=10
nsubcycle=8*1
ncontrol=1
/


&UNITS_PARAMS
units_density = 1.6605390D-24			! g/cm^3 = 1 H / cm^3;	 Ramses unit
units_time    = 3.00386047142391D+15 	! s; -> G=1; needed if poisson=.true.
units_length  = 3.0856776D+21 			! cm = 1 kpc;	Ramses unit
/


&HYDRO_PARAMS
courant_factor=0.5
slope_type=1
scheme='muscl'
riemann='llf'
/


&POISSON_PARAMS
gravity_type=0	! requires to set units such that G=1
/


&AMR_PARAMS
boxlen=6.0d2
levelmin=7
levelmax=14
ngridmax=100000
npartmax=1000000
nexpand=1
/


&REFINE_PARAMS
m_refine=8*25
sink_refine=.true.		! refine around sink to levelmax; default is .false.
interpol_type=0   		! use this to avoid refinement runaway
interpol_var=1    		! use this to avoid refinement runaway
/


&BOUNDARY_PARAMS
nboundary=6
bound_type= 2, 2,  2,  2,  2,  2 ! zero-gradient BCs ...
ibound_min=-1, 1, -1, -1, -1, -1
ibound_max=-1, 1,  1,  1,  1,  1
jbound_min= 0, 0, -1,  1, -1, -1
jbound_max= 0, 0, -1,  1,  1,  1
kbound_min= 0, 0,  0,  0, -1,  1
kbound_max= 0, 0,  0,  0, -1,  1
/


!initfile must be such that expanded by the IC file names
!collectively has len < 80 characters
!the longest file name reads:
!initfile(1)output_xxxxx/hydro_file_descriptor.txt
&INIT_PARAMS
filetype='dice'
!initfile(1)='/scratch/dr86/ttg562/hbd_7_sink/ic/'
initfile(1)='/import/photon2/tepper/hbd_7_sink/ic/''
/


! NOTE: An additional ascii file 'ic_sink' must be present in initfile(1)
! This file contains a list of sinks with the following format
! (see pm/init_sink.f90)
! mass,x,y,z,vx,vy,vz,Lx,Ly,Lz,mass_SMBH,ID
! L is the sinks angular momentum (as in, e.g. an accreting black hole)
&SINK_PARAMS
sink_soft=2 				! default; in units of dx at max.ref.level
mass_sink_direct_force=1.	! mass (Msun) above which sinks are treated with direct N-body solver
create_sinks=.false.		! default, but just in case
mass_sink_seed=1.			! need > 0 for code to run; default is 0
nsinkmax=1					! not sure about this but should save memory
/


!not sure if used when hydro=.false.
&CLUMPFIND_PARAMS
ivar_clump=0				! use DM for clump finding; default is 1 (gas)
/


!MAKE SURE TO USE THE CORRECT UNITS FOR length, velocity, and mass
&DICE_PARAMS
ic_file='hbd_7_preproc.g2'	  ! Name of the initial conditions file
ic_nfile=3                    ! If greater than one, look for files with name matching ic_file//'.n'
ic_ifout=1                    ! Change ramses output index for restarts
ic_format='Gadget2'           ! Format of the initial conditions. 'Gadget1' or 'Gadget2'
gadget_scale_l=3.085677581282D21 ! AGAMA file length unit   -> 1 kpc
gadget_scale_v=1.0D5             ! AGAMA file velocity unit -> 1 km/s
gadget_scale_m=1.9891D33         ! AGAMA file mass unit     -> 1 solar mass
gadget_scale_t=3.15360e+13       ! Gadget file time unit    -> 1 Myr (note that this is NOT the simulation time unit)
/


!output_dir name must have len < 40 characters to accomodate
!the longest Ramses output file names
&OUTPUT_PARAMS
tend = 6.
delta_tout = 0.1
!output_dir='/scratch/dr86/ttg562/hbd_7_sink/'
output_dir='/import/photon2/tepper/hbd_7_sink/'
walltime_hrs=24       ! Wallclock time for submitted job
minutes_dump=10       ! Dump an output minutes before walltime ends
/
