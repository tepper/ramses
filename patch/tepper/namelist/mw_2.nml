! COMPILE CODE WITH: Makefile_3D_GNU_OLDMPI0_DICEMULTIPATCH
&RUN_PARAMS
verbose=.false.
print_when_io=.true.
cosmo=.false.
hydro=.false.
pic=.true.
poisson=.true.	! requires to set units such that G=1; taken care of by units.f90
nrestart=0
nremap=10
nsubcycle=7*1
ncontrol=1
/


&UNITS_PARAMS
units_density = 1.6605390D-24			! g/cm^3 = 1 H / cm^3;	 Ramses unit
units_time    = 3.00386047142391D+15 	! s; -> G=1; needed if poisson=.true.
units_length  = 3.0856776D+21 			! cm = 1 kpc;	Ramses unit
/


&HYDRO_PARAMS
courant_factor=0.5
slope_type=1
scheme='muscl'
riemann='llf'
/


&POISSON_PARAMS
gravity_type=0	! requires to set units such that G=1
/


! Memory management PER CORE (pure N-body runs)
! ~ 0.8*[ (ngridmax/10^6) + (npartmax/10^7) ] GB
! The ICs used here (DICE_PARAMS) have roughly 3.5 million particles
! Note that the more cores, the smaller ngridmax and npartmax can be
&AMR_PARAMS
boxlen=6.0d2
levelmin=8
levelmax=14
ngridmax=200000
npartmax=2000000
nexpand=1
/


&REFINE_PARAMS
m_refine=7*25
interpol_type=0			! use this to avoid refinement runaway
interpol_var=1			! use this to avoid refinement runaway
/


&BOUNDARY_PARAMS
nboundary=6
bound_type= 2, 2,  2,  2,  2,  2 ! zero-gradient BCs ...
ibound_min=-1, 1, -1, -1, -1, -1
ibound_max=-1, 1,  1,  1,  1,  1
jbound_min= 0, 0, -1,  1, -1, -1
jbound_max= 0, 0, -1,  1,  1,  1
kbound_min= 0, 0,  0,  0, -1,  1
kbound_max= 0, 0,  0,  0, -1,  1
/


!initfile must be such that expanded by the IC file names
!collectively has len < 80 characters
!the longest file name reads:
!initfile(1)output_xxxxx/hydro_file_descriptor.txt
&INIT_PARAMS
filetype='dice'
initfile(1)='/scratch/dr86/ttg562/mw_2/ic/'
!initfile(1)='/import/photon2/tepper/mw_2/ic/'
/


!MAKE SURE TO USE THE CORRECT UNITS FOR length, velocity, and mass
!For more parameter see:
!https://bitbucket.org/vperret/dice/wiki/RAMSES%20simulation
&DICE_PARAMS
ic_file='mw_2_proc.g2'				! Name of the initial conditions file
ic_nfile=8							! If greater than one, look for files with name matching ic_file//'.n'
ic_ifout=1							! Change ramses output index for restarts
ic_format='Gadget2'					! Format of the initial conditions. 'Gadget1' or 'Gadget2'
gadget_scale_l=3.085677581282D+21	! AGAMA file length unit   -> 1 kpc
gadget_scale_v=1.0D+5 				! AGAMA file velocity unit -> 1 km/s
gadget_scale_m=1.9891D+33			! AGAMA file mass unit     -> 1 solar mass
gadget_scale_t=3.15360D+13			! Gadget file time unit    -> 1 Myr (note that this is NOT the simulation time unit)
/


!output_dir name must have len < 40 characters to accomodate
!the longest Ramses output file names
&OUTPUT_PARAMS
tend = 20.
delta_tout = 0.1
output_dir='/scratch/dr86/ttg562/mw_2/'
!output_dir='/import/photon2/tepper/mw_2/'
/
