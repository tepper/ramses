Author: Dr. Thorsten Tepper Garcia

Created: 01 JAN 2024


	*IMPORTANT*:
		THIS PATCH STILL HAS A NUMBER OF BUGS!
		For instance, Main step starts at 1 instead of at the actual last value; the same is true for nstep_coarse.
		Restarting a simulation (h_00_gh0_lr_ofe_wh_rs) with the oagertz/dice_restart patch (which derives from this), leads to the mass of evolved, newborn stellar particles drop below their allowed minimum value.


	Last modified:

	16 JAN 2024:
	I've introduced a new RESTART_PARAMS specific parameter, nrestart2, to remove the complication introduced by the complication of having nrestart < 0 as the traditional restart patch requires, in particular the need for patched versions of adaptive_loop.f90 and init_time.f90.
	With this change, the DICE_RESTART is virtually identical to DICE with except of the loading routine in init_part.f90 and some additions to read_params.f90.

	18 JAN 2024:
	Allow to load additional DICE ICs at restart; note that this change implies that load_dice is now linked to load_ramses through two parameters: restart_init and add_dice_ic (new!)
	IMPORTANT: May need to increase npartmax and ngridmax (understandably)
	Replaced 'ramses' to 'ramses_dice' where relevant to distinguish this patch from the original.

	25 JAN 2024:
	Added a counter for particle that are tossed because they are moved beyond the box in the previous time step.
	Removed unnecessary MPI_BCAST clauses.
	Particle tracer pointer has been added BUT NOT YET TESTED!

	20 FEB 2024:
	Remove the use of sink_restart from init_part.f90 as it appeared unnecessary

	28 FEB 2024:
	Added family tag, previously set to 0 by default, now set to the value specified by the ICs (if available)

	20 MAR 2024
	Assign particles actual level 'lev_restart' read from particle file instead of 'levelmin'; particularly relevant for star particles, which are placed at maximum refinement level

TO DO:

	- Remove obsolete bits such as uu for particles, etc.


Notes:

	- When loading additional DICE ICs, there may be a potential issue with duplicate particle ids (iord)
		- ASSES AND FIX

	- Gas particle (cell) number and gas mass increases slightly at restart, presumably due to the fact that IG_rho != 0
		- ASSES AND FIX

	- Particles at or beyond the box boundary will be tossed at restart

About:

This directory contains a patch intended to run DICE simulations
with the capability to restart and include additional DICE initial conditions
at restart.

The base of the patch are the original RESTART patch and the DICE patch (in my case composed of the original patch in patch/init/dice and my patched version in patch/tepper/dice).

The first patch modification consists of an addition of a modified version of the case('ramses') snippet in taken from patch/init/restart/init_part.f90, available via the subroutine load_ramses.

It has been renamed to ramses_dice to distinguish it from the original.
Relevant bits are identified by comments containing 'DICE [patch]' and 'RESTART [patch]'.


USAGE:

In the NML, use:

&RESTART_PARAMS
nrestart2=N			! restart at snapshot N
add_dice_ic=.true.  ! optional; to load additional DICE ICS at restart
restart_vars=6,7   	!-> corresponds to the actual indices of passive scalars (excluding temperature); mandatory when hydro = .true.
/

&INIT_PARAMS
filetype='ramses_dice'
/

&RUN_PARAMS
nrestart=0
/
